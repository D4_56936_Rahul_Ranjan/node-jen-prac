const express = require("express")
const db = require("../db")
const utils = require("../utils")

const router = express.Router()

router.post("/", (request, response) => {
  const { name, salary, age } = request.body

  const query = ` INSERT INTO Employee (name,salary,age) VALUES ('${name}', '${salary}', '${age}') `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get("/", (request, response) => {
  const query = `
        SELECT * FROM Employee `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})
router.put("/:id", (request, response) => {
  const { id } = request.params
  const { name, salary, age } = request.body

  const query = ` UPDATE Employee SET name = '${name}', salary = ${salary}, age = ${age} WHERE id = ${id} `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})
router.delete("/:id", (request, response) => {
  const { id } = request.params

  const query = `
      DELETE FROM Employee
      WHERE
        id = ${id}
    `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
